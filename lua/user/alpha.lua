local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
	return
end

local dashboard = require("alpha.themes.dashboard")
dashboard.section.header.val = {
    [[                                                                              				            ]],
    [[                                                                              				            ]],
    [[                                                                              			            	]],
    [[                                    ██████                                    				            ]],
    [[                                ████▒▒▒▒▒▒████                                				            ]],
    [[                              ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                              			            	]],
    [[                            ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ███╗   ██╗██╗   ██╗██╗███╗   ███╗             ]],
    [[                          ██▒▒▒▒▒▒▒▒    ▒▒▒▒▒▒▒▒            ████╗  ██║██║   ██║██║████╗ ████║             ]],
    [[                          ██▒▒▒▒▒▒  ▒▒▓▓▒▒▒▒▒▒  ▓▓▓▓        ██╔██╗ ██║██║   ██║██║██╔████╔██║             ]],
    [[                          ██▒▒▒▒▒▒  ▒▒▓▓▒▒▒▒▒▒  ▒▒▓▓        ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒    ▒▒▒▒▒▒▒▒    ██      ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██      ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                Javier Pacheco      	        	]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                        				            ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                        				            ]],
    [[                        ██▒▒██▒▒▒▒▒▒██▒▒▒▒▒▒▒▒██▒▒▒▒██                       				                ]],
    [[                        ████  ██▒▒██  ██▒▒▒▒██  ██▒▒██                      				                ]],
    [[                        ██      ██      ████      ████                     				                ]],
    [[                                                                              				            ]],
    [[                                                                              				            ]],
}


dashboard.section.buttons.val = {
	dashboard.button("SPC ff", "  Find file", ":Telescope find_files <CR>"),
	dashboard.button("SPC fn", "  New file", ":ene <BAR> startinsert <CR>"),
	dashboard.button("SPC fr", "  Recently used files", ":Telescope oldfiles <CR>"),
	dashboard.button("SPC ft", "  Find text", ":Telescope live_grep <CR>"),
	dashboard.button("SPC gc", "  Configuration", ":e ~/.config/nvim/init.lua <CR>"),
}

local function footer()
-- NOTE: requires the fortune-mod package to work
	-- local handle = io.popen("fortune")
	-- local fortune = handle:read("*a")
	-- handle:close()
	-- return fortune
	return "“Make it work, make it right, make it fast.” – Kent Beck"
end

dashboard.section.footer.val = footer()

dashboard.section.footer.opts.hl = "Type"
dashboard.section.header.opts.hl = "Include"
dashboard.section.buttons.opts.hl = "Keyword"

dashboard.opts.opts.noautocmd = true
-- vim.cmd([[autocmd User AlphaReady echo 'ready']])
alpha.setup(dashboard.opts)
