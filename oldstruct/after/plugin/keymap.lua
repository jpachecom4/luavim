local Remap = require("user.keymap")
local nnoremap = Remap.nnoremap
local vnoremap = Remap.vnoremap
local inoremap = Remap.inoremap
local xnoremap = Remap.xnoremap
local nmap = Remap.nmap

-- Keybinds
nnoremap("<leader>.", ":Ex<CR>")
nnoremap("<leader>u", ":UndotreeShow<CR>")
nnoremap("<leader>-", ":split<CR>")
nnoremap("<leader>vs", ":vsplit<CR>")
nnoremap("<leader>qp", ":bnext<CR>")
nnoremap("<leader>pq", ":bprev<CR>")

nnoremap("<C-d>", "<C-d>zz")
nnoremap("<C-u>", "<C-u>zz")
nnoremap("<leader>D", ":Alpha<CR>")

-- This is going to get me cancelled
inoremap("<C-c>", "<Esc>")

nnoremap("Q", "<nop>")

nnoremap("<leader>k", "<cmd>lnext<CR>zz")
nnoremap("<leader>j", "<cmd>lprev<CR>zz")

nnoremap("<leader>s", ":%s/\\<<C-r><C-w>\\>//gI<Left><Left><Left>")
nnoremap("<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })

-- Packer binds
nnoremap("<leader><leader>ps", ":PackerSync<CR>")
