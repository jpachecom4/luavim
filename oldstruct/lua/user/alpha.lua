local alpha = require("alpha")
local dashboard = require("alpha.themes.dashboard")


-- Set header
dashboard.section.header.val = {
    [[                                                                              				            ]],
    [[                                                                              				            ]],
    [[                                                                              			            	]],
    [[                                    ██████                                    				            ]],
    [[                                ████▒▒▒▒▒▒████                                				            ]],
    [[                              ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                              			            	]],
    [[                            ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██          ███╗   ██╗██╗   ██╗██╗███╗   ███╗             ]],
    [[                          ██▒▒▒▒▒▒▒▒    ▒▒▒▒▒▒▒▒            ████╗  ██║██║   ██║██║████╗ ████║             ]],
    [[                          ██▒▒▒▒▒▒  ▒▒▓▓▒▒▒▒▒▒  ▓▓▓▓        ██╔██╗ ██║██║   ██║██║██╔████╔██║             ]],
    [[                          ██▒▒▒▒▒▒  ▒▒▓▓▒▒▒▒▒▒  ▒▒▓▓        ██║╚██╗██║╚██╗ ██╔╝██║██║╚██╔╝██║             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒    ▒▒▒▒▒▒▒▒    ██      ██║ ╚████║ ╚████╔╝ ██║██║ ╚═╝ ██║             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██      ╚═╝  ╚═══╝  ╚═══╝  ╚═╝╚═╝     ╚═╝             ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                Javier Pacheco      	        	]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                        				            ]],
    [[                        ██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██                        				            ]],
    [[                        ██▒▒██▒▒▒▒▒▒██▒▒▒▒▒▒▒▒██▒▒▒▒██                       				                ]],
    [[                        ████  ██▒▒██  ██▒▒▒▒██  ██▒▒██                      				                ]],
    [[                        ██      ██      ████      ████                     				                ]],
    [[                                                                              				            ]],
    [[                                                                              				            ]],
}

-- Set menu
dashboard.section.buttons.val = {
    dashboard.button( "ff", "Find file", ":cd ~/docs/ | Telescope find_files<CR>"),
    dashboard.button( "rf", "Recent"   , ":Telescope oldfiles<CR>"),
    dashboard.button( "s", "Settings" , ":e ~/.config/nvim"),
    dashboard.button( "Q", "Quit NVIM", ":qa<CR>"),
}

alpha.setup(dashboard.opts)

-- Disable folding on alpha buffer
vim.cmd([[
    autocmd FileType alpha setlocal nofoldenable
]])

